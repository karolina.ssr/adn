﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
* Projet : ADN
* Détails : calcul du nombre d'occurence des bases ACTG
* Auteur : K. Sauser - CFPT - 2022
* Date : 14.10.2022
* Classe : IDA_P1B
* Version : 1.0
*/

namespace ADN
{
    internal class Program
    {
        static void Main(string[] args)
        {
            StreamReader s;
            s = new StreamReader("G:\\Mon Drive\\2022-2023\\Vendredi\\ADN\\chromosome-11-partial.txt");
            String line;
            line = s.ReadLine();

            //a = a + 0;
            int compterA;
            compterA = 0;

            while (line != null)
            {
                for (int i = 0; i < line.Length; i = i + 1)
                {
                    Console.WriteLine("i=" +1);
                    //Console.WriteLine(line[i])

                    if (line[i] == 'A')
                    {
                        compterA = compterA + 1;
                    }
                    line = s.ReadLine();
                }
                Console.ReadKey();
            }
            Console.WriteLine("compterA=" + compterA);
        }
    }
}
